# High-Tune: Gas Optical Properties

This C library loads the optical properties of a gas mixture stored in
htgop format. See
[gas_opt_prop_en.pdf](https://www.meso-star.com/projects/htrdr/downloads/gas_opt_prop_en.pdf)
for format specification.

## Requirements

- C compiler
- POSIX make
- pkg-config
- [RSys](https://gitlab.com/vaplv/rsys)

## Installation

Edit config.mk as needed, then run:

    make clean install

## Release notes

### Version 0.2

- Replace CMake by Makefile as build system.
- Update compiler and linker flags to increase the security and
  robustness of generated binaries.
- Provide a pkg-config file to link the library as an external
  dependency.

### Version 0.1.2

Sets the CMake minimum version to 3.1: since CMake 3.20, version 2.8 has
become obsolete.

### Version 0.1.1

Remove the hard-coded boundaries of the shortwave/longwave domains.
Actually "shortwave" and "longwave" are only keywords that define that
the source of radiation is whether external or internal to the medium,
respectively.

### Version 0.1

- Add the `htgop_get_<lw|sw>_spectral_intervals` functions: they return
  the indices of the lower and upper spectral intervals that include a
  given range of long/short waves.
- Add the `htgop_find_<lw|sw>_spectral_interval_id` functions: they
  return the index of the spectral interval that includes the submitted
  short/long wave.
- Remove the functions explicitly relying onto the CIE 1931 XYZ color
  space, i.e. `htgop_sample_sw_spectral_interval_CIE_1931_<X|Y|Z>` and
  `htgop_get_sw_spectral_intervals_CIE_XYZ`.

### Version 0.0.2

- Fix an issue when the parsed line is greater than 128 characters.

## Licenses

Copyright (C) 2018-2021, 2023 |Méso|Star> (contact@meso-star.com)

htgop is free software released under the GPL v3+ license: GNU GPL
version 3 or later. You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.


# Copyright (C) 2018-2021, 2023 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = libhtgop.a
LIBNAME_SHARED = libhtgop.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Library building
################################################################################
SRC = src/htgop.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then \
	     echo "$(LIBNAME)"; \
	   else \
	     echo "$(LIBNAME_SHARED)"; \
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(RSYS_LIBS) -lm

$(LIBNAME_STATIC): libhtgop.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

libhtgop.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -DHTGOP_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    htgop.pc.in > htgop.pc

htgop-local.pc: htgop.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    htgop.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" htgop.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/high_tune/" src/htgop.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/htgop" COPYING README.md

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/htgop.pc"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/htgop/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/htgop/README.md"
	rm -f "$(DESTDIR)$(PREFIX)/include/high_tune/htgop.h"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(OBJ) $(TEST_OBJ) $(LIBNAME)
	rm -f .config .test libhtgop.o htgop.pc htgop-local.pc

distclean: clean
	rm -f $(DEP) $(TEST_DEP)
	rm -f etc/ecrad_opt_prop.txt

lint:
	shellcheck -o all make.sh

################################################################################
# Tests
################################################################################
TEST_SRC =\
 src/test_htgop.c\
 src/test_htgop_fetch_radiative_properties.c\
 src/test_htgop_get_radiative_properties_bounds.c\
 src/test_htgop_load.c\
 src/test_htgop_sample.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)
TEST_FILE=etc/ecrad_opt_prop.txt

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
HTGOP_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags htgop-local.pc)
HTGOP_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs htgop-local.pc)

test: build_tests etc/ecrad_opt_prop.txt
	@$(SHELL) make.sh check test_htgop
	@$(SHELL) make.sh check test_htgop_fetch_radiative_properties "$(TEST_FILE)"
	@$(SHELL) make.sh check test_htgop_get_radiative_properties_bounds "$(TEST_FILE)"
	@$(SHELL) make.sh check test_htgop_load "$(TEST_FILE)"
	@$(SHELL) make.sh check test_htgop_sample "$(TEST_FILE)"

build_tests: build_library $(TEST_DEP) .test
	@$(MAKE) -fMakefile -f.test $$(for i in $(TEST_DEP); do echo -f"$${i}"; done) test_bin

.test: Makefile make.sh
	@$(SHELL) make.sh config_test $(TEST_SRC) > $@

clean_test:
	$(SHELL) make.sh clean_test $(TEST_SRC)

$(TEST_DEP): config.mk htgop-local.pc
	@$(CC) $(CFLAGS_EXE) $(HTGOP_CFLAGS) $(RSYS_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

$(TEST_OBJ): config.mk htgop-local.pc
	$(CC) $(CFLAGS_EXE) $(HTGOP_CFLAGS) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

test_htgop \
test_htgop_fetch_radiative_properties \
test_htgop_get_radiative_properties_bounds \
test_htgop_load \
test_htgop_sample \
: config.mk htgop-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(HTGOP_LIBS) $(RSYS_LIBS) -lm

# Touch the target to update its timestamp. Otherwise, the extracted file will
# have a timestamp lower than its prerequisite, forcing its re-extraction.
etc/ecrad_opt_prop.txt: etc.tgz
	tar xzvf etc.tgz && touch $@

/* Copyright (C) 2018-2021, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "htgop.h"
#include "test_htgop_utils.h"

#include <rsys/math.h>
#include <float.h>

/* Generate the check_layer_lw_ka_bounds */
#define DOMAIN lw
#define DATA ka
#include "test_htgop_get_radiative_properties_bounds.h"
/* Generate the check_layer_sw_ka_bounds */
#define DOMAIN sw
#define DATA ka
#include "test_htgop_get_radiative_properties_bounds.h"
/* Generate the check_layer_sw_ks_bounds */
#define DOMAIN sw
#define DATA ks
#include "test_htgop_get_radiative_properties_bounds.h"
/* Generate the check_layer_sw_kext_bounds */
#define GET_K(Tab, Id) ((Tab)->ka_tab[Id] + (Tab)->ks_tab[Id])
#define DOMAIN sw
#define DATA kext
#include "test_htgop_get_radiative_properties_bounds.h"

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct htgop* htgop;

  if(argc < 2) {
    fprintf(stderr, "Usage: %s FILENAME\n", argv[0]);
    return 1;
  }

  CHK(mem_init_proxy_allocator(&allocator, &mem_default_allocator) == RES_OK);
  CHK(htgop_create(NULL, &allocator, 1, &htgop) == RES_OK);
  CHK(htgop_load(htgop, argv[1]) == RES_OK);

  check_layer_lw_ka_bounds(htgop);
  check_layer_sw_ka_bounds(htgop);
  check_layer_sw_ks_bounds(htgop);
  check_layer_sw_kext_bounds(htgop);

  CHK(htgop_ref_put(htgop) == RES_OK);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

/* Copyright (C) 2018-2021, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "htgop.h"
#include "htgop_c.h"

#if !defined(DATA) || !defined(DOMAIN)
  #error "Missing the <DATA|DOMAIN> macro."
#endif

/* Helper macros */
#define GET_NSPECINTS \
  CONCAT(CONCAT(htgop_get_, DOMAIN), _spectral_intervals_count)
#define GET_SPECINT \
  CONCAT(CONCAT(htgop_get_, DOMAIN), _spectral_interval)

#define LAY_SPECINT \
  CONCAT(CONCAT(htgop_layer_, DOMAIN),_spectral_interval)
#define LAY_SPECINT_TAB \
  CONCAT(CONCAT(htgop_layer_, DOMAIN),_spectral_interval_tab)
#define GET_LAY_SPECINT \
  CONCAT(CONCAT(htgop_layer_get_, DOMAIN), _spectral_interval)
#define GET_LAY_SPECINT_TAB \
  CONCAT(CONCAT(htgop_layer_, DOMAIN), _spectral_interval_get_tab)

#define LAY_SPECINT_QUADS_GET_BOUNDS \
  CONCAT(CONCAT(CONCAT(CONCAT( \
    htgop_layer_,DOMAIN),_spectral_interval_quadpoints_get_),DATA),_bounds)
#define LAY_SPECINT_TAB_GET_BOUNDS \
  CONCAT(CONCAT(CONCAT(CONCAT( \
    htgop_layer_,DOMAIN),_spectral_interval_tab_get_),DATA),_bounds)

#ifndef GET_K
  #define GET_K(Tab, Id) ((Tab)->CONCAT(DATA,_tab)[Id])
#endif

static void CONCAT(CONCAT(CONCAT(CONCAT(
check_layer_,DOMAIN),_),DATA),_bounds)(struct htgop* htgop)
{
  struct htgop_layer lay;
  struct LAY_SPECINT band;
  struct LAY_SPECINT_TAB tab;
  double x_h2o_range[2];
  double bounds[2];
  size_t quad_range[2];
  size_t ilay, iband, iquad, itab;
  size_t nlays, nbands;
  
  CHK(GET_NSPECINTS(htgop, &nbands) == RES_OK);
  CHK(htgop_get_layers_count(htgop, &nlays) == RES_OK);

  CHK(nlays && nbands);

  CHK(htgop_get_layer(htgop, 0, &lay) == RES_OK);
  CHK(GET_LAY_SPECINT(&lay, 0, &band) == RES_OK);
  CHK(band.quadrature_length);
  CHK(GET_LAY_SPECINT_TAB(&band, 0, &tab) == RES_OK);

  quad_range[0] = 0;
  quad_range[1] = 0;
  x_h2o_range[0] = 0;
  x_h2o_range[1] = 0;
  CHK(LAY_SPECINT_QUADS_GET_BOUNDS
    (NULL, quad_range, x_h2o_range, bounds) == RES_BAD_ARG);
  CHK(LAY_SPECINT_QUADS_GET_BOUNDS
    (&band, NULL, x_h2o_range, bounds) == RES_BAD_ARG);
  CHK(LAY_SPECINT_QUADS_GET_BOUNDS
    (&band, quad_range, NULL, bounds) == RES_BAD_ARG);
  CHK(LAY_SPECINT_QUADS_GET_BOUNDS
    (&band, quad_range, x_h2o_range, NULL) == RES_BAD_ARG);
  x_h2o_range[0] = 1;
  x_h2o_range[1] = 0;
  CHK(LAY_SPECINT_QUADS_GET_BOUNDS
    (&band, quad_range, x_h2o_range, bounds) == RES_BAD_ARG);
  x_h2o_range[0] = 0;
  x_h2o_range[1] = 1;
  quad_range[0] = 0;
  quad_range[1] = band.quadrature_length;
  CHK(LAY_SPECINT_QUADS_GET_BOUNDS
    (&band, quad_range, x_h2o_range, bounds) == RES_BAD_ARG);

  x_h2o_range[0] = 0;
  x_h2o_range[1] = 0;
  CHK(LAY_SPECINT_TAB_GET_BOUNDS(NULL, x_h2o_range, bounds) == RES_BAD_ARG);
  CHK(LAY_SPECINT_TAB_GET_BOUNDS(&tab, NULL, bounds) == RES_BAD_ARG);
  CHK(LAY_SPECINT_TAB_GET_BOUNDS(&tab, x_h2o_range, NULL) == RES_BAD_ARG);
  x_h2o_range[0] = 1;
  x_h2o_range[1] = 0;
  CHK(LAY_SPECINT_TAB_GET_BOUNDS(&tab, x_h2o_range, bounds) == RES_BAD_ARG);

  FOR_EACH(ilay, 0, nlays) {
    CHK(htgop_get_layer(htgop, ilay, &lay) == RES_OK);

    FOR_EACH(iband, 0, nbands) {
      double quad_bounds2[2] = {DBL_MAX, -DBL_MAX};
      CHK(GET_LAY_SPECINT(&lay, iband, &band) == RES_OK);

      FOR_EACH(iquad, 0, band.quadrature_length) {
        double quad_bounds[2] = {DBL_MAX, -DBL_MAX};
        double tab_bounds2[2] = {DBL_MAX, -DBL_MAX};
        size_t itest;

        CHK(GET_LAY_SPECINT_TAB(&band, iquad, &tab) == RES_OK);

        FOR_EACH(itab, 0, tab.tab_length) {
          const double k = GET_K(&tab, itab);
          double tab_bounds[2] = {DBL_MAX, -DBL_MAX};
          tab_bounds2[0] = MMIN(tab_bounds2[0], k);
          tab_bounds2[1] = MMAX(tab_bounds2[1], k);

          x_h2o_range[0] = tab.x_h2o_tab[0];
          x_h2o_range[1] = tab.x_h2o_tab[itab];
          CHK(LAY_SPECINT_TAB_GET_BOUNDS
            (&tab, x_h2o_range, tab_bounds) == RES_OK);
          CHK(eq_eps(tab_bounds2[0], tab_bounds[0], 1.e-6));
          CHK(eq_eps(tab_bounds2[1], tab_bounds[1], 1.e-6));
        }

        FOR_EACH(itest, 0, 10) {
          const double bias = tab.x_h2o_tab[0];
          const double scale = tab.x_h2o_tab[tab.tab_length-1] - bias;
          const double r0 = rand_canonic();
          const double r1 = rand_canonic();
          double tab_bounds[2] = {DBL_MAX, -DBL_MAX};
          double tab_bounds3[2] = {DBL_MAX, -DBL_MAX};
          double k0, k1;
          size_t tab_range[2];

          x_h2o_range[0] = r0*scale + bias;
          x_h2o_range[1] = r1*scale + bias;
          if(x_h2o_range[0] > x_h2o_range[1]) {
            SWAP(double, x_h2o_range[0], x_h2o_range[1]);
          }
          HTGOP(CONCAT(CONCAT(CONCAT(
            layer_,DOMAIN),_spectral_interval_tab_fetch_),DATA)
              (&tab, x_h2o_range[0], &k0));
          HTGOP(CONCAT(CONCAT(CONCAT(
            layer_,DOMAIN),_spectral_interval_tab_fetch_),DATA)
              (&tab, x_h2o_range[1], &k1));

          tab_bounds[0] = MMIN(k0, k1);
          tab_bounds[1] = MMAX(k0, k1);

          tab_range[0] = SIZE_MAX;
          tab_range[1] = SIZE_MAX;
          FOR_EACH(itab, 0, tab.tab_length) {
            if(tab_range[0]==SIZE_MAX && tab.x_h2o_tab[itab] >  x_h2o_range[0])
              tab_range[0] = itab;
            if(tab_range[1]==SIZE_MAX && tab.x_h2o_tab[itab] >= x_h2o_range[1])
              tab_range[1] = itab;
            if(tab_range[0] != SIZE_MAX && tab_range[1] != SIZE_MAX)
              break;
          }
          CHK(tab_range[0] != SIZE_MAX && tab_range[1] != SIZE_MAX);

          FOR_EACH(itab, tab_range[0], tab_range[1]) {
            tab_bounds[0] = MMIN(GET_K(&tab, itab), tab_bounds[0]);
            tab_bounds[1] = MMAX(GET_K(&tab, itab), tab_bounds[1]);
          }

          CHK(LAY_SPECINT_TAB_GET_BOUNDS
            (&tab, x_h2o_range, tab_bounds3) == RES_OK);
          CHK(eq_eps(tab_bounds[0], tab_bounds3[0], 1.e-6));
          CHK(eq_eps(tab_bounds[1], tab_bounds3[1], 1.e-6));
        }

        quad_range[0] = iquad;
        quad_range[1] = iquad;
        x_h2o_range[0] = lay.x_h2o_tab[0];
        x_h2o_range[1] = lay.x_h2o_tab[lay.tab_length-1];
        CHK(LAY_SPECINT_QUADS_GET_BOUNDS
          (&band, quad_range, x_h2o_range, quad_bounds) == RES_OK);
        CHK(eq_eps(quad_bounds[0], tab_bounds2[0], 1.e-6));
        CHK(eq_eps(quad_bounds[1], tab_bounds2[1], 1.e-6));

        quad_bounds2[0] = MMIN(quad_bounds[0], quad_bounds2[0]);
        quad_bounds2[1] = MMAX(quad_bounds[1], quad_bounds2[1]);

        quad_range[0] = 0;
        quad_range[1] = iquad;
        CHK(LAY_SPECINT_QUADS_GET_BOUNDS
          (&band, quad_range, x_h2o_range, quad_bounds) == RES_OK);
        CHK(eq_eps(quad_bounds2[0], quad_bounds[0], 1.e-6));
        CHK(eq_eps(quad_bounds2[1], quad_bounds[1], 1.e-6));
      }
    }
  }
}

#undef GET_NSPECINTS
#undef GET_SPECINT
#undef LAY_SPECINT_QUADS_GET_BOUNDS
#undef LAY_SPECINT_TAB_GET_BOUNDS
#undef LAY_SPECINT
#undef LAY_SPECINT_TAB
#undef GET_LAY_SPECINT
#undef GET_LAY_SPECINT_TAB
#undef GET_K
#undef DOMAIN
#undef DATA

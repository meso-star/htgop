/* Copyright (C) 2018-2021, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "htgop.h"
#include <math.h>

#if !defined(DOMAIN) || !defined(DATA)
  #error "Missing the <DATA|DOMAIN> macro."
#endif

/* Helper macros */
#define GET_NSPECINTS \
  CONCAT(CONCAT(htgop_get_, DOMAIN), _spectral_intervals_count)
#define GET_SPECINT \
  CONCAT(CONCAT(htgop_get_, DOMAIN), _spectral_interval)
#define LAY_SPECINT \
  CONCAT(CONCAT(htgop_layer_, DOMAIN),_spectral_interval)
#define LAY_SPECINT_TAB \
  CONCAT(CONCAT(htgop_layer_, DOMAIN),_spectral_interval_tab)
#define GET_LAY_SPECINT \
  CONCAT(CONCAT(htgop_layer_get_, DOMAIN), _spectral_interval)
#define GET_LAY_SPECINT_TAB \
  CONCAT(CONCAT(htgop_layer_, DOMAIN), _spectral_interval_get_tab)
#define FETCH_K \
  CONCAT(CONCAT(CONCAT(htgop_layer_, DOMAIN),_spectral_interval_tab_fetch_), DATA)

#ifndef GET_K
  #define GET_K(Tab, Id) ((Tab)->CONCAT(DATA, _tab)[Id])
#endif

static void
CONCAT(CONCAT(CONCAT(check_layer_fetch_, DOMAIN),_), DATA)
  (const struct htgop* htgop,
   const struct htgop_layer* layer)
{
  struct htgop_spectral_interval specint;
  size_t ispecint;
  size_t nspecints;
  double k;

  CHK(htgop && layer);
  CHK(GET_NSPECINTS(htgop, &nspecints) == RES_OK);
  CHK(nspecints);

  CHK(GET_SPECINT(htgop, 0, &specint) == RES_OK);
  CHK(specint.quadrature_length);
  CHK(layer->tab_length);

  FOR_EACH(ispecint, 0, nspecints) {
    struct LAY_SPECINT lay_specint;
    struct LAY_SPECINT_TAB lay_tab;
    size_t iquad;

    CHK(GET_SPECINT(htgop, ispecint, &specint) == RES_OK);
    CHK(specint.quadrature_length);

    FOR_EACH(iquad, 0, specint.quadrature_length) {
      size_t itab;

      CHK(GET_LAY_SPECINT(layer, ispecint, &lay_specint) == RES_OK);
      CHK(GET_LAY_SPECINT_TAB(&lay_specint, iquad, &lay_tab) == RES_OK);
      CHK(lay_tab.tab_length == layer->tab_length);

      FOR_EACH(itab, 0, layer->tab_length) {
        double x_h2o;
        const size_t N = 10;
        size_t i;

        /* Check boundaries */
        x_h2o = layer->x_h2o_tab[itab];
        CHK(FETCH_K(&lay_tab, x_h2o, &k) == RES_OK);
        CHK(eq_eps(k, GET_K(&lay_tab,itab), 1.e-6));

        FOR_EACH(i, 0, N) {
          const double r = rand_canonic();
          const double x1 = itab ? layer->x_h2o_tab[itab-1] : 0;
          const double x2 = layer->x_h2o_tab[itab];
          const double k1 = itab ? GET_K(&lay_tab, itab-1) : 0;
          const double k2 = GET_K(&lay_tab, itab);
          double ref;

          x_h2o = x1 + (x2 - x1) * r;
          if(!itab || !k1 || !k2) {
            ref = k1 + (k2-k1) / (x2 - x1) * x_h2o;
          } else {
            const double alpha = (log(k2) - log(k1)) / (log(x2) - log(x1));
            const double beta = log(k1) - alpha*log(x1);
            ref = exp(alpha*log(x_h2o) + beta);
          }

          CHK(FETCH_K(&lay_tab, x_h2o, &k) == RES_OK);
          CHK(eq_eps(k, ref, 1.e-6));
#ifdef Kext
          {
            double ka, ks;
            CHK(FETCH_Ka(&lay_tab, x_h2o, &ka) == RES_OK);
            CHK(FETCH_Ks(&lay_tab ,x_h2o, &ks) == RES_OK);
            CHK(ka <= k && ks <= k);
          }
#endif
        }
      }
    }
  }
}

#undef GET_NSPECINTS
#undef GET_SPECINT
#undef LAY_SPECINT
#undef LAY_SPECINT_TAB
#undef GET_LAY_SPECINT
#undef GET_LAY_SPECINT_TAB
#undef GET_K
#undef DOMAIN
#undef DATA

